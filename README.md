# _Тепловая карта мезонина_

устновка зависимостей с помощью npm
```sh
npm install
```
Запуск dev версии
```sh
npm run dev
```
Сборка продакшн версии в папку /dist (это то, что выгружаем на сервер (хостинг))
```sh
npm run build
```
## Настройка сервера
установка nginx - https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-22-04
python обычно установлен везде, если нет то по стандартной инструкии
далее находим дирректорию /var/www/html и закидываем туда файлы продакшена из папки /dist

чтобы роутинг vue работал правильно, нужно настроить nginx:
```sh
nano /etc/nginx/sites-enabled/default
```
видим строку `server_name _;` и за ней видим `location / {что-то там..}` необходимо заменить содержимое на:
```sh
try_files $uri $uri/ /index.html;
add_header 'Access-Control-Allow-Origin' '*';
add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS';
add_header 'Access-Control-Allow-Headers' 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range';
add_header 'Access-Control-Expose-Headers' 'Content-Length,Content-Range';
add_header 'Access-Control-Allow-Credentials' 'true';
```
это настроит сервер для правильного роуту vue и отключит защиту CORS (данные `data.json` можно будет запросить с любого url)

установить зависимости python:
```sh
pip install requests
```

теперь необходимо настроить цикличный запуск скрипта `parser_wb.py`
```sh
nano crontab -e
```
пишием в конце файла новые строки
```sh
*/30 * * * * rm /var/www/html/data.json
*/30 * * * * /usr/bin/python3 /var/www/html/parser_wb.py
```
подмечу, что там указывается путь до python `/usr/bin/python3`. если у вас что-то другое, всегда можно узнать путь командой `whereis python3`

файлы которые должны находится в дирректории `/var/www/html`:
```sh
config.py
parser_wb.py
файлы продакшн версии из папки /dist
```

файл `config.py` это конфигурационный файл относящийся к `parser_wb.py`. в нем хранятся номер авторизации и пароль

# Контакты
связаться со мной можно по ссылкам:
[WhatSapp 79886126261](https://wa.me/+79886126261/)
[Telegram s_unityy](https://t.me/s_unityy)