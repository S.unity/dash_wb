import requests
import json
import time

from config import phone, password

session = requests.Session()
urlLogin = 'https://reports.wildberries.ru/srv/auth_phone_password/api/login'
dataLogin = {
           "password": password,
           "phone_number": phone
       }
response = session.post(urlLogin, json=dataLogin)
print('Аутентификация прошла успешно')

################################################################# kazan1 #################################
database = {
    "kazan1": {
        "1": [],
        "2": [],
        "3": [],
        "4": [],
        "5": [],
        "6": []
    },
    "kazan1_safe": {
        "113": []
    },
    "kazan2": {
        "1": [],
        "2": [],
        "3": [],
        "4": [],
        "5": [],
        "6": []
    },
    "kazan2_safe": {
        "113": []
    },
    "kazan4": {
        "1": [],
        "2": [],
        "3": [],
        "4": [],
        "5": [],
        "6": []
    },
    "kazan4_food": {
        "2": []
    }
}
print('Получаю данные...')
url = 'https://reports.wildberries.ru/srv/reports_layout_proxy_adapter/sql/shk_mezo_volume_layer3'
data = {
           "wh_id": 76,
           "stage": 1
       }

################################################################# kazan1 #################################
response = session.post(url, json=data)
database["kazan1"]["1"].append(response.json())

data = {
           "wh_id": 76,
           "stage": 2
       }
response = session.post(url, json=data)
database["kazan1"]["2"].append(response.json())
data = {
           "wh_id": 76,
           "stage": 3
       }
response = session.post(url, json=data)
database["kazan1"]["3"].append(response.json())
data = {
           "wh_id": 76,
           "stage": 4
       }
response = session.post(url, json=data)
database["kazan1"]["4"].append(response.json())
data = {
           "wh_id": 76,
           "stage": 5
       }
response = session.post(url, json=data)
database["kazan1"]["5"].append(response.json())
data = {
           "wh_id": 76,
           "stage": 6
       }
response = session.post(url, json=data)
database["kazan1"]["6"].append(response.json())

################################################################# kazan1_safe #################################
data = {
           "wh_id": 117,
           "stage": 113
       }
response = session.post(url, json=data)
database["kazan1_safe"]["113"].append(response.json())

################################################################# kazan2 #################################
data = {
           "wh_id": 83,
           "stage": 1
       }
response = session.post(url, json=data)
database["kazan2"]["1"].append(response.json())
data = {
           "wh_id": 83,
           "stage": 2
       }
response = session.post(url, json=data)
database["kazan2"]["2"].append(response.json())
data = {
           "wh_id": 83,
           "stage": 3
       }
response = session.post(url, json=data)
database["kazan2"]["3"].append(response.json())
data = {
           "wh_id": 83,
           "stage": 4
       }
response = session.post(url, json=data)
database["kazan2"]["4"].append(response.json())
data = {
           "wh_id": 83,
           "stage": 5
       }
response = session.post(url, json=data)
database["kazan2"]["5"].append(response.json())
data = {
           "wh_id": 83,
           "stage": 6
       }
response = session.post(url, json=data)
database["kazan2"]["6"].append(response.json())

################################################################# kazan2_safe #################################
data = {
           "wh_id": 150,
           "stage": 113
       }
response = session.post(url, json=data)
database["kazan2_safe"]["113"].append(response.json())

################################################################# kazan4 #################################
data = {
           "wh_id": 112,
           "stage": 1
       }
response = session.post(url, json=data)
database["kazan4"]["1"].append(response.json())
data = {
           "wh_id": 112,
           "stage": 2
       }
response = session.post(url, json=data)
database["kazan4"]["2"].append(response.json())
data = {
           "wh_id": 112,
           "stage": 3
       }
response = session.post(url, json=data)
database["kazan4"]["3"].append(response.json())
data = {
           "wh_id": 112,
           "stage": 4
       }
response = session.post(url, json=data)
database["kazan4"]["4"].append(response.json())
data = {
           "wh_id": 112,
           "stage": 5
       }
response = session.post(url, json=data)
database["kazan4"]["5"].append(response.json())
data = {
           "wh_id": 112,
           "stage": 6
       }
response = session.post(url, json=data)
database["kazan4"]["6"].append(response.json())

################################################################# kazan4_food #################################
data = {
           "wh_id": 291,
           "stage": 2
       }
response = session.post(url, json=data)
database["kazan4_food"]["2"].append(response.json())



with open('/var/www/html/data.json', 'w', encoding='utf-8') as f:
    json.dump(database, f, ensure_ascii=False, indent=4)

print('Данные получены и обновлены')