import { createRouter, createMemoryHistory, createWebHistory } from 'vue-router';

const isServer = typeof window === 'undefined';
const history = isServer ? createMemoryHistory() : createWebHistory();
const routes = [
    {
        path: '/',
        name: 'App',
        component: ()=> import('../App.vue'),
    }
];
const router = createRouter({
    history,
    routes,
});

export default router;
